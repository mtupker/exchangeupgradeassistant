﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.NetworkInformation;
using ExchangeUpgradeAssistant;
using System.Management;
using System.Windows.Forms;

namespace UpStatus
{
    class Status
    {
        //http://stackoverflow.com/questions/13430926/check-status-of-the-server-continuously
        //http://msdn.microsoft.com/en-us/library/system.net.networkinformation.ping(v=vs.110).aspx
        public static bool GetPingResponse(string args, int timeout = 1000)
        {
            try
            {
                Ping ping = new Ping();
                PingReply reply = ping.Send(args, timeout);
                if (reply.Status == IPStatus.Success)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (PingException err)
            {
                //MessageBox.Show("An error occurred: " + err.Message);
                return false;
            }
        }
    }
}
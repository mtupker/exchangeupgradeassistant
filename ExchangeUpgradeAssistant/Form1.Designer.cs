﻿namespace ExchangeUpgradeAssistant
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_SaveSettings = new System.Windows.Forms.Button();
            this.textBox_domain_name = new System.Windows.Forms.TextBox();
            this.label1_configlocation = new System.Windows.Forms.Label();
            this.button_getVersion = new System.Windows.Forms.Button();
            this.tabPage1_Exchange = new System.Windows.Forms.TabPage();
            this.listView_Serverinfo = new System.Windows.Forms.ListView();
            this.columnHeader_samaccountname = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader1_version = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader1_up = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.button_checkstatus = new System.Windows.Forms.Button();
            this.tabPage1_Exchange.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button_SaveSettings
            // 
            this.button_SaveSettings.Location = new System.Drawing.Point(0, 0);
            this.button_SaveSettings.Name = "button_SaveSettings";
            this.button_SaveSettings.Size = new System.Drawing.Size(75, 23);
            this.button_SaveSettings.TabIndex = 0;
            // 
            // textBox_domain_name
            // 
            this.textBox_domain_name.Location = new System.Drawing.Point(0, 0);
            this.textBox_domain_name.Name = "textBox_domain_name";
            this.textBox_domain_name.Size = new System.Drawing.Size(100, 20);
            this.textBox_domain_name.TabIndex = 0;
            // 
            // label1_configlocation
            // 
            this.label1_configlocation.Location = new System.Drawing.Point(0, 0);
            this.label1_configlocation.Name = "label1_configlocation";
            this.label1_configlocation.Size = new System.Drawing.Size(100, 23);
            this.label1_configlocation.TabIndex = 0;
            // 
            // button_getVersion
            // 
            this.button_getVersion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button_getVersion.Location = new System.Drawing.Point(17, 546);
            this.button_getVersion.Name = "button_getVersion";
            this.button_getVersion.Size = new System.Drawing.Size(96, 58);
            this.button_getVersion.TabIndex = 1;
            this.button_getVersion.Text = "Get Installed Version";
            this.button_getVersion.UseVisualStyleBackColor = true;
            this.button_getVersion.Click += new System.EventHandler(this.button_getVersion_Click);
            // 
            // tabPage1_Exchange
            // 
            this.tabPage1_Exchange.Controls.Add(this.listView_Serverinfo);
            this.tabPage1_Exchange.Location = new System.Drawing.Point(4, 22);
            this.tabPage1_Exchange.Name = "tabPage1_Exchange";
            this.tabPage1_Exchange.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1_Exchange.Size = new System.Drawing.Size(924, 500);
            this.tabPage1_Exchange.TabIndex = 0;
            this.tabPage1_Exchange.Text = "Exchange";
            this.tabPage1_Exchange.UseVisualStyleBackColor = true;
            // 
            // listView_Serverinfo
            // 
            this.listView_Serverinfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listView_Serverinfo.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader_samaccountname,
            this.columnHeader1_version,
            this.columnHeader1_up});
            this.listView_Serverinfo.GridLines = true;
            this.listView_Serverinfo.Location = new System.Drawing.Point(6, 6);
            this.listView_Serverinfo.Name = "listView_Serverinfo";
            this.listView_Serverinfo.Size = new System.Drawing.Size(912, 488);
            this.listView_Serverinfo.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.listView_Serverinfo.TabIndex = 0;
            this.listView_Serverinfo.UseCompatibleStateImageBehavior = false;
            this.listView_Serverinfo.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader_samaccountname
            // 
            this.columnHeader_samaccountname.Text = "Server";
            this.columnHeader_samaccountname.Width = 125;
            // 
            // columnHeader1_version
            // 
            this.columnHeader1_version.Text = "Version";
            this.columnHeader1_version.Width = 125;
            // 
            // columnHeader1_up
            // 
            this.columnHeader1_up.Text = "Up";
            this.columnHeader1_up.Width = 125;
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1_Exchange);
            this.tabControl1.Location = new System.Drawing.Point(13, 13);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(932, 526);
            this.tabControl1.TabIndex = 0;
            // 
            // button_checkstatus
            // 
            this.button_checkstatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button_checkstatus.Location = new System.Drawing.Point(120, 546);
            this.button_checkstatus.Name = "button_checkstatus";
            this.button_checkstatus.Size = new System.Drawing.Size(96, 58);
            this.button_checkstatus.TabIndex = 2;
            this.button_checkstatus.Text = "Check Status";
            this.button_checkstatus.UseVisualStyleBackColor = true;
            this.button_checkstatus.Click += new System.EventHandler(this.button_checkstatus_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(957, 616);
            this.Controls.Add(this.button_checkstatus);
            this.Controls.Add(this.button_getVersion);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "Exchange Upgrade Assistant";
            this.tabPage1_Exchange.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox textBox_domain_name;
        private System.Windows.Forms.Label label1_configlocation;
        private System.Windows.Forms.Button button_SaveSettings;
        private System.Windows.Forms.Button button_getVersion;
        private System.Windows.Forms.TabPage tabPage1_Exchange;
        private System.Windows.Forms.ListView listView_Serverinfo;
        private System.Windows.Forms.ColumnHeader columnHeader_samaccountname;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.ColumnHeader columnHeader1_version;
        private System.Windows.Forms.ColumnHeader columnHeader1_up;
        private System.Windows.Forms.Button button_checkstatus;
    }
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.DirectoryServices;
using System.Management;
using System.IO;
using UpStatus;

namespace ExchangeUpgradeAssistant
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            reset_form();
        }

        private void reset_form()  //blanks out form
        {
            listView_Serverinfo.Items.Clear();
        }

        private void button_getVersion_Click(object sender, EventArgs e)
        {
            try
            {
                get_ExchangeVersion();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
        }

        private void get_ExchangeVersion()  //determine installed exchange build from AD
        {
            try
            {
                reset_form();

                //connect to AD and search for servers.
                DirectoryEntry RootDSE = new DirectoryEntry("LDAP://RootDSE");
                DirectoryEntry ConfigContainer = new DirectoryEntry("LDAP://" + RootDSE.Properties["configurationNamingContext"].Value);

                //search AD for exchange servers. added !objectClass=msExchExchangeTransportServer to support exchange 2013 CAS servers roles since they register Frontend as msExchExchangeServer
                DirectorySearcher ConfigSearcher = new DirectorySearcher(ConfigContainer);
                ConfigSearcher.Filter = "(&(objectClass=msExchExchangeServer)(!objectClass=msExchClientAccessArray)(!objectClass=msExchExchangeTransportServer))";
                ConfigSearcher.PropertiesToLoad.Add("name");
                ConfigSearcher.PropertiesToLoad.Add("serialNumber");
                ConfigSearcher.PropertiesToLoad.Add("samAccountName");
                ConfigSearcher.PageSize = 10000;
                ConfigSearcher.SearchScope = SearchScope.Subtree;

                //loop though results and update listview with server names and versions (serialNumber stores exchange version)
                foreach (SearchResult result in ConfigSearcher.FindAll())
                {
                    ListViewItem item = new ListViewItem();
                    //below line need for color of ping result
                    item.UseItemStyleForSubItems = false;
                    item.Text = result.Properties["name"][0].ToString();
                    item.SubItems.Add(result.Properties["serialNumber"][0].ToString());
                    item.SubItems.Add("");

                    listView_Serverinfo.Items.Add(item);
                }
                listView_Serverinfo.EndUpdate();
            }
            catch (Exception err)
            {
                MessageBox.Show("An error occurred while querying for Exchange server information: " + err.Message);
            }
        }

        private void button_checkstatus_Click(object sender, EventArgs e)
        {
            //get the current exchange versions before pinging servers
            get_ExchangeVersion();
            //loop thought listview entries and ping the server names. update listview to show status.
            foreach (ListViewItem listview_item in listView_Serverinfo.Items) 
            {
               if (Status.GetPingResponse(listview_item.SubItems[0].Text.ToString()))
               {
                   listview_item.SubItems[2].Text = "UP";
                   listview_item.SubItems[2].BackColor = Color.Green;
               }
               else
               {
                   listview_item.SubItems[2].Text = "DOWN";
                   listview_item.SubItems[2].BackColor = Color.Red;
               }
            }
        }
    }
}